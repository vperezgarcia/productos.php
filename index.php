<?php  
//Conexion
include('includes/conexion.php');

//incluyo las clases de forma manual
// include('includes/classes/producto.class.php');
// include('includes/classes/menu.class.php');
// include('includes/classes/imagen.class.php');
// include('includes/classes/jumbotron.class.php');
// include('includes/classes/calendario.class.php');

//Incluyo las clases de forma automatica

$directorioClases='includes/classes/';
$dir=opendir($directorioClases);
while ($archivo=readdir($dir)) {
  if (is_file($directorioClases.$archivo)) {
    include($directorioClases.$archivo);
  }
}
closedir($dir);


if (isset($_GET['p'])) {
  $p=$_GET['p'];
}else{
  $p='inicio.php';
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Tienda de clase</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap THEME -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <section class="container">
      <header class="page-header">
        <?php 
        include('includes/encabezado.php');
        ?>
       </header>
       <nav>
         <?php 
         include ('includes/menu.php');
         ?>
       </nav>

      <div class="row">

        <nav class="col-md-3">
          <?php
            include('includes/menucategorias.php');
            include('includes/menuenlaces.php');
          ?>
        </nav>

        <section class="col-md-9">
          <?php include ('paginas/'. $p);?>
        </section>

      </div>

       <footer>
         <?php
         include('includes/pie.php');
         ?>
       </footer>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<?php  
  //desconecto de la base de datos
  mysqli_close($conexion);
?>