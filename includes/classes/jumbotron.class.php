<?php 

class Jumbotron{

	public $titulo;
	public $texto;
	public $textoEnlace;
	public $urlEnlace;
	
	function __construct(){
		$this->titulo='';
		$this->texto='';
		$this->textoEnlace='';
		$this->urlEnlace='';


		
	}

	function dibujame(){
		$resultado='';
		$resultado.='<hr>';
		$resultado.='<div class="jumbotron">';
		$resultado.='<div class="container">';

		$resultado.='<h1>';
		$resultado.=$this->titulo;
		$resultado.='</h1>';

		$resultado.='<p>';
		$resultado.=$this->texto;
		$resultado.='</p>';

		$resultado.='<a class="btn btn-primary btn-lg" role="button" href="'.$this->urlEnlace.'">'.$this->textoEnlace.'';

		$resultado.='</a>';

		$resultado.='</div>';

		$resultado.='</div>';

		return $resultado;
	}
}

 ?>