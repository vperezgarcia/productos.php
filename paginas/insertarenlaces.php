<h4>Nuevo Enlace</h4>

<form role="form" method="post" action="index.php?p=insercionenlaces.php">
	
	<div class="form-group">
		<label for="descripcionEn">Nombre del Enlace</label>
		<input type="text" class="form-control" id="descripcionEn" name="descripcionEn" placeholder="Introduce el nombre del enlace">
	</div>

	<div class="form-group">
		<label for="direccionEn">Dirección del Enlace</label>
		<input type="text" class="form-control" id="direccionEn" name="direccionEn" placeholder="Introduce la direccion del enlace">
	</div>

	<div class="form-group">
		<input type="submit" class="form-control" name="insertar" value="Nuevo enlace">
	</div>

</form>