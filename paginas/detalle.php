<?php
//Fichero:	paginas/detalle.php
//Funcion:	Recibirá el id del producto, y mostrará la info de dicho producto

//Tengo que recoger el id de producto que me han mandado

$id=$_GET['id'];

//Confecciono la pregunta a mi base de datos

$sql="SELECT * FROM productos WHERE idProd=$id";

//Ejecuto la consulta

$consulta=mysqli_query($conexion, $sql);

//Extraigo un unico resultado

$r=mysqli_fetch_array($consulta);
?>

<article>
	<header>
		<h2> <?php echo $r['nombreProd'];?> </h2>
	</header>
	<section>
		<p> <?php echo $r['descripcionProd'];?> </p>
	</section>
	<footer>
		<h4> <?php echo $r['precioProd'];?> Euros</h4>
		<h4> Quedan <?php echo $r['unidadesProd'];?> Unidades en Stock</h4>
	</footer>
	<a href="index.php?p=listado.php">Volver</a>
</article>

<hr>

<form role="form" method="post" action="index.php?p=detalle.php&id=<?php echo $id;?>" enctype="multipart/form-data">

	<div class="form-group">
			<label for="nombreProd">Nombre de la imagen</label>
			<input type="text" class="form-control" id="descripcionImg" name="descripcionImg" placeholder="Introduce el nombre de la imagen">
		</div>

		<input type="hidden" name="idProd" value="<?php echo $id; ?>">

	<div class="form-group">
			<label for="ArchivoImg">Archivo de imagen</label>
			<input type="file" class="form-control" id="archivoImg" name="archivoImg">
		</div>

		<div class="form-group">
			<input type="submit" class="form-control" name="insertar" value="Alta de producto">
		</div>
	</form>


<?php 

//Procesar la informacion que enviamos desde el formulario
if (isset($_POST['insertar'])) {
	$descripcionImg=$_POST['descripcionImg'];
	$idProd=$_POST['idProd'];

	//$_FILES['archivoImg']['name']; Nombre del fichero
	//$_FILES['archivoImg']['tmp_name']; Ubicacion temporal
	//$_FILES['archivoImg']['size']; Tamaño en BYTES
	//$_FILES['archivoImg']['type']; ej: image/jpeg
	//$_FILES['archivoImg']['error']; Codigo de error

	$nombreImagen=$_FILES['archivoImg']['name'];

	move_uploaded_file($_FILES['archivoImg']['tmp_name'], 'imagenes/'.$nombreImagen);

	$sqlImg="INSERT INTO imagenes(descripcionImg, archivoImg, idProd)VALUES('$descripcionImg', '$nombreImagen', '$idProd')";

	$consultaImg=mysqli_query($conexion, $sqlImg);
}

?>

<hr>

<?php

//Creo una consulta para mostrar imagenes del producto actual
$sql="SELECT * FROM imagenes WHERE idProd=$id";

//Ejecuto la consulta
$consulta=mysqli_query($conexion, $sql);

//Recorro los resultados de la consulta
while ($r=mysqli_fetch_array($consulta)) {
	?>
	<figure style="display: inline-block;">
		<img src="imagenes/<?php echo $r['archivoImg'];?>" width="200">
		<figcaption>
			<?php echo $r['descripcionImg'];?>
		</figcaption>
	</figure>
	<?php
}

?>