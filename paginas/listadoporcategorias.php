<br>
<h4>Listado de Productos por Categoria</h4>

<?php 
//Recogemos el idCat de la barra de direcciones
$idCat=$_GET['idCat'];
//Pensamos la pregunta para sacar la info de idCat
$sql="SELECT * FROM categorias WHERE idCat=$idCat";
//ejecutamos la pregunta (consulta)
$consulta=mysqli_query($conexion, $sql);
//Extraemos el UNICO resultado
$r=mysqli_fetch_array($consulta);
?>

<div class="page-header">
<h4><?php echo $r['nombreCat']?>
	</div>
	<small><?php echo $r['descripcionCat']?></small>
	<img src="imagenes/categorias/<?php echo $r['imagenCat'];?>" alt="" width="200" class="img-responsive">
</h4>

<hr>
<section class="row">
<?php

//Comprobamos que idCat es un numero
if (is_numeric($idCat)) {

	//Pensamos la pregunta para sacar la info de producto de esa categoria
	$sql="SELECT * FROM productos WHERE idCat=$idCat";
	//ejecutamos la pregunta (consulta)
	$consulta=mysqli_query($conexion, $sql);
	//Voy a contar resultados, si hay mas de 0, los muestro
	if (mysqli_num_rows($consulta)) {
	//Extraemos los posibles resultados
	while ($r=mysqli_fetch_array($consulta)) {
		?>
		<article class="col-md-3 col-sm-6">
			<header>
				<h3>
					<?php echo $r['nombreProd'];?>
				</h3>
			</header>
			<section>
				<?php echo $r['descripcionProd'];?>
			</section>
		</article>
		<?php
		}
		?>
		</section>
		<?php
	}else{
		echo "Lo siento, no hay productos en esta categoria";
	}

}
?>